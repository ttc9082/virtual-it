__author__ = 'Tianqi Tong'


import boto
import time
import os
import datetime
import random
import sys

my_access_key = "" # Access Key Here
my_secret_key = "" # Secret Key Here
my_groupname = "ttc_group"
my_cidr_ip = '0.0.0.0/0'
my_key_path = "c:\\pem"
my_key_name = "ttc_key"
pem_extension = '.pem'
my_key = 'NULL'
#Emp is a List of Employee object of class EmpData
Emp=[]
conn = boto.connect_ec2(aws_access_key_id=my_access_key, aws_secret_access_key=my_secret_key)
def CreateGroup():
    print 'Creating A Group.'
    try:
        print 'Creating security group %s.' % my_groupname
        secure_group = conn.create_security_group(my_groupname, 'ttc')
        try:
            print 'adding SSH port to the group.'
            secure_group.authorize("tcp", 22, 22, my_cidr_ip)
            secure_group.authorize("tcp", 80, 80, my_cidr_ip)
            secure_group.authorize("tcp", 0, 65535, my_cidr_ip)
        except conn.ResponseError, e:
            if e.code == 'InvalidPermission.Duplicate':
                print 'The %s is already authorized.' % my_groupname
            else:
                raise
        print 'group authorized..'
    except conn.ResponseError, e:
        if e.code == 'InvalidGroup.Duplicate':
            print 'The Group is already exist.'
        else:
            raise
    print "SecurityGroup Initiated."

def CreateKeypair():
    print 'Creating A Keypair.'
    try:
        print 'Creating key pair %s.' % my_key_name
        key_pair = conn.create_key_pair(my_key_name)
        key_path = key_pair.save(my_key_path)
    except conn.ResponseError, e:
        if e.code == 'InvalidKeyPair.Duplicate':
            print 'The KeyPair %s is existed.' % my_key_name
        else:
            raise
    print 'Keypair Initiated.'
    my_key = os.path.join(my_key_path, my_key_name) + pem_extension
    return my_key

def CreateInstance(AMI='ami-7341831a'):
    Reserv_Instance = conn.run_instances(AMI,
                                             key_name=my_key_name,
                                             security_groups=[my_groupname],
                                             instance_type='t1.micro',
                                             placement='us-east-1d'
        )
    My_Instance = Reserv_Instance.instances[0]
    print 'waiting for instance..'
    while My_Instance.state != 'running':
        time.sleep(5)
        print '.'
        My_Instance.update()
    print 'creating instance succeed!'
    return My_Instance

def TerminateInstance(My_Instance):
    print 'Instance will Terminate in 5s.'
    for i in range(1,5):
        time.sleep(1)
        c = 5-i
        print '.', c
    My_Instance.terminate()

def ClearGroupAndKeypair(My_Instance,my_key= my_key):
    print 'now wait for the instance terminated, so that we can delete the security group and the key pair.'
    while My_Instance.state !='terminated':
        time.sleep(5)
        print '.'
        My_Instance.update()
    print 'Instance terminated.'
    conn.delete_security_group(name=my_groupname)
    conn.delete_key_pair(my_key_name)
    os.remove(my_key)
    print 'the key pair %s and the secure group %s has been deleted.' % (my_key_name, my_groupname)

def CreateAndAttachVolume(My_Instance, size='1'):
    Volume = conn.create_volume(size, My_Instance.placement)
    while Volume.status != 'available':
        time.sleep(5)
        print 'Waiting for the volume to be Available.'
        Volume.update()
    print 'volume is available'
    Volume.attach(My_Instance.id, '/dev/sdh')
    while Volume.status != 'in-use':
        print 'Volume Attaching'
        print Volume.attach
        time.sleep(5)
        Volume.update()
    print 'Volume %s is attached to Instance %s' % (Volume.id, My_Instance.id)
    return Volume.id

def ReAttachVolume(My_Instance, volume):
    My_Volume = conn.get_all_volumes(volume)[0]
    My_Volume.attach(My_Instance.id, '/dev/sdh')
    while My_Volume.status != 'in-use':
        print 'Volume Attaching'
        print My_Volume.attach
        time.sleep(5)
        My_Volume.update()
    print 'Volume %s is attached to Instance %s' % (My_Volume.id, My_Instance.id)

def DetachVolume(volume):
    print 'trying to detach the volume.'
    Volume = conn.get_all_volumes(volume)[0]
    Volume.detach()
    print 'volume detached.'
    return volume

def CreateS3(name):
    S3 = boto.connect_s3(aws_access_key_id=my_access_key, aws_secret_access_key=my_secret_key)
    Bucket = 'empty'
    while Bucket == 'empty':
        try:
            Bucket = S3.create_bucket(name)
        except boto.exception.S3CreateError, e:
            if e.code == 'BucketAlreadyExists':
                name = raw_input('The name is already exist. Enter a new name manually :')
            else:
                raise
            print 'trying to create again.'
    print 'S3 storage %s created successfully!' % Bucket.name
    print 'Writing a hello.txt in the volume.'
    key = Bucket.new_key('hello.txt')
    key.set_contents_from_string('Hello World!')
    print 'Done.'
    print 'tring to read hello.txt.'
    key.get_contens_as_string()
    print 'done.'
    return Bucket.name

def AllocateEIP():
    EIP = conn.allocate_address()
    return EIP.public_ip

def AssociateIP(My_Instance, EIP):
    Eip = conn.get_all_addresses(EIP)[0]
    Eip.associate(My_Instance.id)
    print '%s EIP %s is associated to %s' % (My_Instance.ip_address, Eip.public_ip, My_Instance.id)
    return EIP

def Disassociate(EIP):
    print 'trying to disassociated EIP.'
    Eip = conn.get_all_addresses(EIP)[0]
    result = Eip.disassociate()
    print 'EIP disassociated!'
    return result

def CreateAMI(My_Instance):
    name = time.strftime('%Y%m%d%H%M%S')
    My_AMI = My_Instance.create_image(name)
    while conn.get_all_images(My_AMI)[0].state != 'available':
        print 'Wait for the AMI. It may take about 1 minutes.'
        time.sleep(10)
        conn.get_all_images(My_AMI)[0].update()
    print "AMI %s now is available." % My_AMI
    return My_AMI

def DelAMI(AMI):
    print 'Trying to deregister previous AMI.'
    conn.get_all_images(AMI)[0].deregister(True)
    print "AMI %s is now deleted."

def CloudWatch(My_Instance):
    end = datetime.datetime.utcnow()
    start = end - datetime.timedelta(minutes=10)
    cloudconn = boto.connect_cloudwatch(my_access_key,my_secret_key)
    Cloud = cloudconn.get_metric_statistics('300',
                                            start,
                                            end,
                                            metric_name='CPUUtilization',
                                            namespace='AWS/EC2',
                                            statistics=['Average'],
                                            dimensions={'InstanceId':My_Instance.id})
    return Cloud

def Bexist():
    for i in range(len(Emp)):
        if Emp[i].Emp == 'ForBalancing':
            print 'found balancing instance.'
            return i
        else:
            print 'No balancing instance found.'
        return False

def Check_1(num):
    Cloud = []
    k = 0
    while len(Cloud) == False:
        Cloud = CloudWatch(Emp[num].Instance)
        time.sleep(10)
        print 'wait for statistics.'
    for i in range(len(Cloud)):
        k = k+Cloud[i]['Average']
        s = k/len(Cloud)
    print 'In the last 10 minutes, the average percentage of CPUUtilization is %s' % s
    print s
    if s <= 0.5:
        print 'No.1 computer is Idle for 5 minutes, going to shutdown.'
        Shutdown(num)
        return 'Idle'
    if s >= 80:
        print 'CPUUtilization is too high, creating a new instance to balance load.'
        print 'create 1 instance at one time.'
        print 'make a AMI and Copy the volume to make a "same instance".'
        AMI = CreateAMI(Emp[num].Instance)
        Instance1 = CreateInstance(AMI)
        Volume = conn.get_all_volumes(Emp[num].volume)[0]
        snapshot = Volume.create_snapshot('for_copy')
        new_volume = snapshot.create_volume(Emp[num].Instance.placement)
        Emp.append(EmpData(Emp='ForBalancing', Instance=Instance1, Volume=new_volume.id))
        NO = len(Emp)-1
        Emp[NO].show()
        print 'An instance that used for balancing CPUUtilization is created, status listed above.'
        return 'added'
    if s <= 20 and Bexist() != False:
        TerminateInstance(Emp[Bexist()].Instance)
        print 'Instance for balancing terminated.'
        return 'reduced'

def Check_2(num):
    Cloud = []
    k = 0
    while len(Cloud) == False :
        Cloud = CloudWatch(Emp[num].Instance)
        time.sleep(10)
        print 'wait for statistics.'
    for i in range(len(Cloud)):
        k = k+Cloud[i]['Average']
        s = k/len(Cloud)
    print 'In the last 10 minutes, the average percentage of CPUUtilization is %s' % s
    print s
    if s <= 0.5:
        print 'No.1 computer is Idle for 5 minutes, going to shutdown.'
        Shutdown(num)
        return 'Idle'

class EmpData:
    def __init__(self,Emp='NULL', AMI='NULL', Instance='NULL', EIP='NULL', Volume='NULL', S3='NULL'):
        self.AMI = AMI
        self.Emp = Emp
        self.Instance = Instance
        self.EIP = EIP
        self.Volume = Volume
        self.S3 = S3
    def show(self):

        print "Employee:\nAMI: %s\nInstance: %s\nIP: %s\ndisk: %s\n" \
              "S3: %s\nstate: %s" % (self.AMI, self.Instance.id, self.EIP, self.Volume, self.S3,self.Emp)

def InitiateInstance(N='NULL', AMI='ami-7341831a'):
    #Create N instance with EIP and EBS and a CloudWatch is Initialized automatically
    if N == 'NULL':
        N = input('Enter how many instances you want to initiate: ')
    for i in range(N):
        CreateGroup()
        my_key = CreateKeypair()
        Instance1 = CreateInstance(AMI)
        Volume1 = CreateAndAttachVolume(Instance1)
        EIP1 = AllocateEIP()
        AssociateIP(Instance1,EIP1)
        Name1 = 'volume' + str(random.randint(1000, 9999))
        S3 = CreateS3(name=Name1)
        Emp.append(EmpData(Emp='Initialized', Instance=Instance1, EIP=EIP1, Volume=Volume1,S3=S3))
        num = len(Emp)-1
        Emp[num].show()
        print 'Initialized %s instance(s).' % (i+1)

def Shutdown(shut='NULL'):
    # Shutdown instance
    while shut == 'NULL':
        shut = input('Please enter the employee No. '
                         'of the computer that you want to shutdown'
                         '(Now you have from No.0 to No.%s): ' % (len(Emp)-1))
        if Emp[shut].Emp == 'Shutdown':
            print 'It is already shutdown!'
            time.sleep(5)
            return False
        else:
            print 'ready.'
            break
    DetachVolume(Emp[shut].Volume)
    Disassociate(Emp[shut].EIP)
    AMI1 = CreateAMI(Emp[shut].Instance)
    while Emp[shut].AMI != "NULL":
        DelAMI(Emp[shut].AMI)
        break
    Emp[shut].AMI = AMI1
    TerminateInstance(Emp[shut].Instance)
    Emp[shut].Emp = 'Shutdown'
    Emp[shut].show()
    print 'shutdown succeed!'

def Start():
    while 1:
        start = input('Please enter the employee No.'
                      ' you want to start'
                      '(Now you have from No.0 to No.%s ):' % (len(Emp)-1))
        if Emp[start].Emp == 'Running':
            print 'It is already running.'
        elif Emp[start].AMI == 'NULL':
            print 'no AMI to restart computer.'
        else:
            print 'ready.'
            break
    Emp[start].Instance = CreateInstance(Emp[start].AMI)
    Emp[start].Volume = ReAttachVolume(Emp[start].Instance, Emp[start].Volume)
    Emp[start].EIP = AssociateIP(Emp[start].Instance, Emp[start].EIP)
    Emp[start].Emp = 'Running'
    Emp[start].show()
    print 'Start succeed!'

def Homework():
    InitiateInstance(2)
    No_1 = len(Emp)-2
    No_2 = len(Emp)-1
    while int(time.strftime('%H')) < 17:
        if Emp[No_1].Emp == 'Shutdown' and Emp[No_2].Emp == 'Shutdown':
            print 'no computer is running now.'
            return True
        Check_1(No_1)
        Check_2(No_2)
        print 'Now it is before 5PM. And computers are not Idle. Will check 5 minutes later. '
        time.sleep(300)
    print "It's after 5pm. You can go home ~"
    Shutdown(No_1)
    Shutdown(No_2)

def Choose(x):
    {1:InitiateInstance, 2:Shutdown, 3:Start, 4:ClearGroupAndKeypair, 5:bye, 6:Homework}[x]()

def bye():
    print 'Bye Bye!'

def console():
    x = 0
    while x != 5:
        print '**************************************************'
        print '*****                                        *****'
        print '*****        Welcome to the Console!         *****'
        print '*****         Author : Tianqi Tong           *****'
        print '********----------------------------------********'
        print '*****                                        *****'
        print '*****   Please Choose What You Want To Do!   *****'
        print '*****                                        *****'
        print '********----------------------------------********'
        print '*****     Enter 1 : Initialize Instances     *****'
        print '*****     Enter 2 : Shutdown Computers       *****'
        print '*****     Enter 3 : Start Computers          *****'
        print '*****     Enter 4 : Clear stuff              *****'
        print '*****     Enter 5 : Quit The Console         *****'
        print '*****     Enter 6 : Assignment 1 SHOW WORK   *****'
        print '**************************************************'
        print '**************************************************'
        print '**************************************************'

        x = input('Enter here :')
        Choose(x)

console()


